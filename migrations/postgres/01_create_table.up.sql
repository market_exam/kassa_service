
CREATE TABLE IF NOT EXISTS "smena" (
    "id"          UUID PRIMARY KEY,
    "smena_code"    VARCHAR(32) NOT NULL,
    "filial_id"   UUID NOT NULL,
    "employee_id" UUID NOT NULL,
    "sale_point_id" UUID NOT NULL,
    "status"      VARCHAR(10) DEFAULT 'new',
    "created_at"  TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at"  TIMESTAMP,
    "deleted_at"  TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "transaction" (
    "id"          UUID PRIMARY KEY,
    "smena_id"    UUID NOT NULL REFERENCES smena(id),
    "cash"        NUMERIC DEFAULT 0,
    "uzcard"      NUMERIC DEFAULT 0, 
    "humo"        NUMERIC DEFAULT 0,
    "apelsin"     NUMERIC DEFAULT 0,
    "payme"       NUMERIC DEFAULT 0,
    "visa"        NUMERIC DEFAULT 0,
    "click"       NUMERIC DEFAULT 0,
    "total_sum"   NUMERIC DEFAULT 0,
    "created_at"  TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at"  TIMESTAMP,
    "deleted_at"  TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "sales" (
    "id"          UUID PRIMARY KEY,
    "sale_code"   VARCHAR(32) NOT NULL,
    "smena_id"    UUID NOT NULL REFERENCES smena(id),
    "filial_id"   UUID NOT NULL,
    "sale_point_id" UUID NOT NULL,
    "employee_id" UUID NOT NULL,
    "status"      VARCHAR(10) DEFAULT 'new',
    "created_at"  TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at"  TIMESTAMP,
    "deleted_at"  TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "sale_products" (
    "id"          UUID PRIMARY KEY,
    "sale_id"     UUID NOT NULL REFERENCES sales(id),
    "category_id" UUID NOT NULL,
    "product_id"  UUID NOT NULL,
    "filial_id"   UUID NOT NULL,
    "barcode"     VARCHAR(50),
    "ostatok_count" INT,
    "count"       INT,
    "price_without_discount" NUMERIC,
    "discount"    NUMERIC,
    "discount_type" VARCHAR(50),
    "price"       NUMERIC,
    "total_price" NUMERIC,
    "created_at"  TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at"  TIMESTAMP,
    "deleted_at"  TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "payment" (
    "id"          UUID PRIMARY KEY,
    "sale_id"     UUID NOT NULL REFERENCES sales(id),
    "cash"        NUMERIC DEFAULT 0,
    "uzcard"      NUMERIC DEFAULT 0, 
    "humo"        NUMERIC DEFAULT 0,
    "apelsin"     NUMERIC DEFAULT 0,
    "payme"       NUMERIC DEFAULT 0,
    "visa"        NUMERIC DEFAULT 0,
    "click"       NUMERIC DEFAULT 0,
    "total_sum"   NUMERIC DEFAULT 0,
    "created_at"  TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at"  TIMESTAMP,
    "deleted_at"  TIMESTAMP
);