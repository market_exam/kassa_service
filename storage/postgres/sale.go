package postgres

import (
	"context"
	"database/sql"
	"errors"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/spf13/cast"

	"app/genproto/kassa_service"
	"app/pkg/helper"
	"app/storage"
)

type SaleRepo struct {
	db *pgxpool.Pool
}

func NewSaleRepo(db *pgxpool.Pool) storage.SaleRepoI {
	return &SaleRepo{
		db: db,
	}
}

func (c *SaleRepo) Create(ctx context.Context, req *kassa_service.CreateSale) (resp *kassa_service.SalePrimaryKey, err error) {


	var sale int

	c.db.QueryRow(ctx, `SELECT COALESCE(COUNT(*), 0) FROM smena WHERE status = 'open' AND filial_id = $1`, req.FilialId).Scan(&sale)

	if sale <= 0 {
		return nil, errors.New("У вас нет открытая смена")
	}

	

	var id = uuid.New()

	smena_id := req.SaleCode

	smena_id += "-0000"
	var temp int

	err = c.db.QueryRow(ctx, `SELECT COALESCE(COUNT(*), 0) + 1 FROM sales`).Scan(&temp)
	if err != nil {
		return nil, err
	}

	smena_id += cast.ToString(temp)

	query := `INSERT INTO "sales" (
				id,
				sale_code,
				smena_id,
				filial_id,
				sale_point_id,
				employee_id,
				status,
				updated_at
			) VALUES ($1, $2, $3, $4, $5, $6, $7, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		smena_id,
		req.GetSmenaId(),
		req.GetFilialId(),
		req.GetSalePointId(),
		req.GetEmployeeId(),
		req.GetStatus(),
	)

	if err != nil {
		return nil, err
	}

	return &kassa_service.SalePrimaryKey{Id: id.String()}, nil
}

func (c *SaleRepo) GetByPKey(ctx context.Context, req *kassa_service.SalePrimaryKey) (resp *kassa_service.Sale, err error) {

	query := `
		SELECT
			id,
			sale_code,
			smena_id,
			filial_id,
			sale_point_id,
			employee_id,
			status,
			created_at,
			updated_at,
			deleted_at
		FROM "sales"
		WHERE id = $1 and deleted_at is null
	`

	var (
		id            sql.NullString
		sale_code     sql.NullString
		smena_id      sql.NullString
		filial_id     sql.NullString
		sale_point_id sql.NullString
		employee_id   sql.NullString
		status        sql.NullString
		createdAt     sql.NullString
		updatedAt     sql.NullString
		deleteddAt    sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.GetId()).Scan(
		&id,
		&sale_code,
		&smena_id,
		&employee_id,
		&filial_id,
		&sale_point_id,
		&status,
		&createdAt,
		&updatedAt,
		&deleteddAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &kassa_service.Sale{
		SaleId:      id.String,
		SaleCode:    sale_code.String,
		SmenaId:     smena_id.String,
		EmployeeId:  employee_id.String,
		FilialId:    filial_id.String,
		SalePointId: sale_point_id.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
		DeletedAt:   deleteddAt.String,
	}

	return
}

func (c *SaleRepo) GetAll(ctx context.Context, req *kassa_service.GetListSaleRequest) (resp *kassa_service.GetListSaleResponse, err error) {

	resp = &kassa_service.GetListSaleResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " AND TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			sale_code,
			smena_id,
			filial_id,
			sale_point_id,
			employee_id,
			status,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
			COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
		FROM "sales"
		WHERE COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')=''
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var sale kassa_service.Sale

		err := rows.Scan(
			&resp.Count,
			&sale.SaleId,
			&sale.SaleCode,
			&sale.SmenaId,
			&sale.FilialId,
			&sale.SalePointId,
			&sale.EmployeeId,
			&sale.Status,
			&sale.CreatedAt,
			&sale.UpdatedAt,
			&sale.DeletedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Sales = append(resp.Sales, &sale)
	}

	return
}

func (c *SaleRepo) Update(ctx context.Context, req *kassa_service.UpdateSale) (rowsAffected int64, err error) {

	query := `
			UPDATE
			    "sales"
			SET
				filial_id     = $1,
				sale_point_id = $2,
				employee_id   = $3,
				status        = $4,
				sale_code 	  = $5,
				updated_at = now()
			WHERE
				id = $6`

	result, err := c.db.Exec(ctx, query,
		req.GetFilialId(),
		req.GetSalePointId(),
		req.GetEmployeeId(),
		req.GetStatus(),
		req.GetSaleCode(),
		req.GetSaleId(),
	)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *SaleRepo) Delete(ctx context.Context, req *kassa_service.SalePrimaryKey) error {

	query := `DELETE FROM "sales" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
