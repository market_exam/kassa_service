package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"app/genproto/kassa_service"
	"app/pkg/helper"
	"app/storage"
)

type SalePaymentRepo struct {
	db *pgxpool.Pool
}

func NewSalePaymentRepo(db *pgxpool.Pool) storage.SalePaymentRepoI {
	return &SalePaymentRepo{
		db: db,
	}
}

func (c *SalePaymentRepo) Create(ctx context.Context, req *kassa_service.CreateSalePayment) (resp *kassa_service.SalePaymentPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "payment" (
				id,
				sale_id,
				cash,
				uzcard,
				humo,
				apelsin,
				payme,
				visa,
				click,
				total_sum,
				updated_at
			) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.GetSaleId(),
		req.GetCash(),
		req.GetUzcard(),
		req.GetHumo(),
		req.GetApelsin(),
		req.GetPayme(),
		req.GetVisa(),
		req.GetClick(),
		req.GetTotalSum(),
	)

	if err != nil {
		return nil, err
	}

	return &kassa_service.SalePaymentPrimaryKey{Id: id.String()}, nil
}

func (c *SalePaymentRepo) GetByPKey(ctx context.Context, req *kassa_service.SalePaymentPrimaryKey) (resp *kassa_service.SalePayment, err error) {

	query := `
		SELECT
			id,
			sale_id,
			cash,
			uzcard,
			humo,
			apelsin,
			payme,
			visa,
			click,
			total_sum,
			created_at,
			updated_at,
			deleted_at
		FROM "payment"
		WHERE id = $1 and deleted_at is null
	`

	var (
		id         sql.NullString
		sale_id    sql.NullString
		cash       sql.NullFloat64
		uzcard     sql.NullFloat64
		humo       sql.NullFloat64
		apelsin    sql.NullFloat64
		payme      sql.NullFloat64
		visa       sql.NullFloat64
		click      sql.NullFloat64
		total_sum  sql.NullFloat64
		createdAt  sql.NullString
		updatedAt  sql.NullString
		deleteddAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.GetId()).Scan(
		&id,
		&sale_id,
		&cash,
		&uzcard,
		&humo,
		&apelsin,
		&payme,
		&visa,
		&click,
		&total_sum,
		&createdAt,
		&updatedAt,
		&deleteddAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &kassa_service.SalePayment{
		Id: id.String,
		SaleId:        sale_id.String,
		Cash:          cash.Float64,
		Uzcard:        uzcard.Float64,
		Apelsin:       apelsin.Float64,
		Payme:         payme.Float64,
		Visa:          visa.Float64,
		Click:         click.Float64,
		Humo:          humo.Float64,
		TotalSum:      total_sum.Float64,
		CreatedAt:     createdAt.String,
		UpdatedAt:     updatedAt.String,
		DeletedAt:     deleteddAt.String,
	}

	return
}

func (c *SalePaymentRepo) GetAll(ctx context.Context, req *kassa_service.GetListSalePaymentRequest) (resp *kassa_service.GetListSalePaymentResponse, err error) {

	resp = &kassa_service.GetListSalePaymentResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " AND TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			sale_id,
			cash,
			uzcard,
			humo,
			apelsin,
			payme,
			visa,
			click,
			total_sum,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
			COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
		FROM "payment"
		WHERE COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')=''
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var payment kassa_service.SalePayment

		err := rows.Scan(
			&resp.Count,
			&payment.Id,
			&payment.SaleId,
			&payment.Cash,
			&payment.Uzcard,
			&payment.Humo,
			&payment.Apelsin,
			&payment.Payme,
			&payment.Visa,
			&payment.Click,
			&payment.TotalSum,
			&payment.CreatedAt,
			&payment.UpdatedAt,
			&payment.DeletedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.SalePayments = append(resp.SalePayments, &payment)
	}

	return
}

func (c *SalePaymentRepo) Update(ctx context.Context, req *kassa_service.UpdateSalePayment) (rowsAffected int64, err error) {

	query := `
			UPDATE
			    "payment"
			SET
				id = $1,
				sale_id = $2,
				cash = $3,
				uzcard = $4,
				humo = $5, 
				apelsin = $6,
				payme = $7,
				visa = $8,
				click = $9,
				total_sum = $10,
				updated_at = now()
			WHERE
				id = $11`

	result, err := c.db.Exec(ctx, query,
		req.GetId(),
		req.GetSaleId(),
		req.GetCash(),
		req.GetUzcard(),
		req.GetHumo(),
		req.GetApelsin(),
		req.GetPayme(),
		req.GetVisa(),
		req.GetClick(),
		req.GetTotalSum(),
		req.GetId(),
	)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *SalePaymentRepo) Delete(ctx context.Context, req *kassa_service.SalePaymentPrimaryKey) error {

	// query := `DELETE FROM "products" WHERE product_id = $1`

	query := `UPDATE "payment" SET deleted_at = now() WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.GetId())

	if err != nil {
		return err
	}

	return nil
}
