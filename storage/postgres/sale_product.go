package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"app/genproto/kassa_service"
	"app/pkg/helper"
	"app/storage"
)

type SaleProductRepo struct {
	db *pgxpool.Pool
}

func NewSaleProductRepo(db *pgxpool.Pool) storage.SaleProductRepoI {
	return &SaleProductRepo{
		db: db,
	}
}

func (c *SaleProductRepo) Create(ctx context.Context, req *kassa_service.CreateSaleProduct) (resp *kassa_service.SaleProductPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "sale_products" (
				id,
				sale_id,
				category_id,
				product_id,
				filial_id,
				barcode,
				ostatok_count,
				count,
				price_without_discount,
				discount,
				discount_type,
				price,
				total_price,
				updated_at
			) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.GetSaleId(),
		req.GetCategoryId(),
		req.GetProductId(),
		req.GetProductId(),
		req.GetBarcode(),
		req.GetOstatokCount(),
		req.GetCount(),
		req.GetPriceWithoutDiscount(),
		req.GetDiscount(),
		req.GetDiscountType(),
		req.GetPrice(),
		req.GetTotalPrice(),
	)

	if err != nil {
		return nil, err
	}

	return &kassa_service.SaleProductPrimaryKey{Id: id.String()}, nil
}

func (c *SaleProductRepo) GetByPKey(ctx context.Context, req *kassa_service.SaleProductPrimaryKey) (resp *kassa_service.SaleProduct, err error) {

	query := `
		SELECT
			id,
			sale_id,
			category_id,
			product_id,
			filial_id,
			barcode,
			ostatok_count,
			count,
			price_without_discount,
			discount,
			discount_type,
			price,
			total_price,
			created_at,
			updated_at,
			deleted_at
		FROM "sale_products"
		WHERE id = $1 and deleted_at is null
	`

	var (
		id                     sql.NullString
		sale_id                sql.NullString
		category_id            sql.NullString
		product_id             sql.NullString
		filial_id              sql.NullString
		barcode                sql.NullString
		ostatok_count          sql.NullFloat64
		count                  sql.NullFloat64
		price_without_discount sql.NullFloat64
		discount               sql.NullFloat64
		discount_type          sql.NullString
		price                  sql.NullFloat64
		total_price            sql.NullFloat64
		createdAt              sql.NullString
		updatedAt              sql.NullString
		deleteddAt             sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.GetId()).Scan(
		&id,
		&sale_id,
		&category_id,
		&product_id,
		&filial_id,
		&barcode,
		&ostatok_count,
		&count,
		&price_without_discount,
		&discount,
		&discount_type,
		&price,
		&total_price,
		&createdAt,
		&updatedAt,
		&deleteddAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &kassa_service.SaleProduct{
		Id:        id.String,
		SaleId:               sale_id.String,
		CategoryId:           category_id.String,
		ProductId:            product_id.String,
		Barcode:              barcode.String,
		OstatokCount:         int32(ostatok_count.Float64),
		Count:                int32(count.Float64),
		PriceWithoutDiscount: price_without_discount.Float64,
		Discount:             discount.Float64,
		DiscountType:         discount_type.String,
		Price:                price.Float64,
		TotalPrice:           total_price.Float64,
		CreatedAt:            createdAt.String,
		UpdatedAt:            updatedAt.String,
		DeletedAt:            deleteddAt.String,
	}

	return
}

func (c *SaleProductRepo) GetAll(ctx context.Context, req *kassa_service.GetListSaleProductRequest) (resp *kassa_service.GetListSaleProductResponse, err error) {

	resp = &kassa_service.GetListSaleProductResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " AND TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			sale_id,
			category_id,
			product_id,
			filial_id,
			barcode,
			ostatok_count,
			count,
			price_without_discount,
			discount,
			discount_type,
			price,
			total_price,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
			COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
		FROM "sale_products"
		WHERE COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')=''
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var saleProduct kassa_service.SaleProduct

		err := rows.Scan(
			&resp.Count,
			&saleProduct.Id,
			&saleProduct.SaleId,
			&saleProduct.CategoryId,
			&saleProduct.ProductId,
			&saleProduct.FilialId,
			&saleProduct.Barcode,
			&saleProduct.OstatokCount,
			&saleProduct.Count,
			&saleProduct.PriceWithoutDiscount,
			&saleProduct.Discount,
			&saleProduct.DiscountType,
			&saleProduct.Price,
			&saleProduct.TotalPrice,
			&saleProduct.CreatedAt,
			&saleProduct.UpdatedAt,
			&saleProduct.DeletedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.SaleProducts = append(resp.SaleProducts, &saleProduct)
	}

	return
}

func (c *SaleProductRepo) Update(ctx context.Context, req *kassa_service.UpdateSaleProduct) (rowsAffected int64, err error) {

	query := `
			UPDATE
			    "sale_products"
			SET
				id = $1,
				sale_id = $2,
				category_id = $3,
				product_id = $4,
				filial_id  = $5,
				barcode = $6, 
				ostatok_count = $7,
				count = $8,
				price_without_discount = $9,
				discount = $10,
				discount_type = $11,
				price = $12,
				total_price = $13,
				updated_at = now()
			WHERE
				id = $14`

	result, err := c.db.Exec(ctx, query,
		req.GetId(),
		req.GetSaleId(),
		req.GetCategoryId(),
		req.GetProductId(),
		req.GetFilialId(),
		req.GetBarcode(),
		req.GetOstatokCount(),
		req.GetCount(),
		req.GetPriceWithoutDiscount(),
		req.GetDiscount(),
		req.GetDiscountType(),
		req.GetPrice(),
		req.GetTotalPrice(),
		req.GetId(),
	)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *SaleProductRepo) Delete(ctx context.Context, req *kassa_service.SaleProductPrimaryKey) error {

	// query := `DELETE FROM "products" WHERE product_id = $1`

	query := `DELETE FROM "sale_products" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.GetId())

	if err != nil {
		return err
	}

	return nil
}
