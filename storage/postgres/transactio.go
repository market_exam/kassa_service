package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"app/genproto/kassa_service"
	"app/pkg/helper"
	"app/storage"
)

type TransactionRepo struct {
	db *pgxpool.Pool
}

func NewTransactionRepo(db *pgxpool.Pool) storage.TransactionRepoI {
	return &TransactionRepo{
		db: db,
	}
}

func (c *TransactionRepo) Create(ctx context.Context, req *kassa_service.CreateTransaction) (resp *kassa_service.TransactionPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "transaction" (
				id,
				smena_id,
				updated_at
			) VALUES ($1, $2, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.GetSmenaId(),
	)

	if err != nil {
		return nil, err
	}

	return &kassa_service.TransactionPrimaryKey{Id: id.String()}, nil
}

func (c *TransactionRepo) GetByPKey(ctx context.Context, req *kassa_service.TransactionPrimaryKey) (resp *kassa_service.Transaction, err error) {

	query := `
		SELECT
			id,
			smena_id,
			cash,
			uzcard,
			humo,
			apelsin,
			payme,
			visa,
			click,
			total_sum,
			created_at,
			updated_at,
			deleted_at
		FROM "transaction"
		WHERE id = $1 and deleted_at is null
	`

	var (
		id         sql.NullString
		smena_id   sql.NullString
		cash       sql.NullFloat64
		uzcard     sql.NullFloat64
		humo       sql.NullFloat64
		apelsin    sql.NullFloat64
		payme      sql.NullFloat64
		visa       sql.NullFloat64
		click      sql.NullFloat64
		total_sum  sql.NullFloat64
		createdAt  sql.NullString
		updatedAt  sql.NullString
		deleteddAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.GetId()).Scan(
		&id,
		&smena_id,
		&cash,
		&uzcard,
		&humo,
		&apelsin,
		&payme,
		&visa,
		&click,
		&total_sum,
		&createdAt,
		&updatedAt,
		&deleteddAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &kassa_service.Transaction{
		Id:        id.String,
		SmenaId:   smena_id.String,
		Cash:      cash.Float64,
		Uzcard:    uzcard.Float64,
		Apelsin:   apelsin.Float64,
		Payme:     payme.Float64,
		Visa:      visa.Float64,
		Click:     click.Float64,
		Humo:      humo.Float64,
		TotalSum:  total_sum.Float64,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
		DeletedAt: deleteddAt.String,
	}

	return
}

func (c *TransactionRepo) GetAll(ctx context.Context, req *kassa_service.GetListTransactionRequest) (resp *kassa_service.GetListTransactionResponse, err error) {

	resp = &kassa_service.GetListTransactionResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " AND TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			smena_id,
			cash,
			uzcard,
			humo,
			apelsin,
			payme,
			visa,
			click,
			total_sum,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
			COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
		FROM "transaction"
		WHERE COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')=''
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var transaction kassa_service.Transaction

		err := rows.Scan(
			&resp.Count,
			&transaction.Id,
			&transaction.SmenaId,
			&transaction.Cash,
			&transaction.Uzcard,
			&transaction.Humo,
			&transaction.Apelsin,
			&transaction.Payme,
			&transaction.Visa,
			&transaction.Click,
			&transaction.TotalSum,
			&transaction.CreatedAt,
			&transaction.UpdatedAt,
			&transaction.DeletedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Transactions = append(resp.Transactions, &transaction)
	}

	return
}

// func (c *TransactionRepo) Update(ctx context.Context, req *kassa_service.UpdateTransaction) (rowsAffected int64, err error) {

// 	query := `
// 			UPDATE
// 			    "smena"
// 			SET
// 				smena_id = $1,
// 				employee_id = $2,
// 				branch_id = $3,
// 				store_id = $4,
// 				updated_at = now()
// 			WHERE
// 				smena_id = $5`

// 	result, err := c.db.Exec(ctx, query,
// 		req.GetSmenaId(),
// 		req.GetEmployeeId(),
// 		req.GetBranchId(),
// 		req.GetStoreId(),
// 		req.GetSmenaId(),
// 	)
// 	if err != nil {
// 		return
// 	}

// 	return result.RowsAffected(), nil
// }

func (c *TransactionRepo) Delete(ctx context.Context, req *kassa_service.TransactionPrimaryKey) error {

	query := `DELETE FROM "transaction" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
