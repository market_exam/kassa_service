package postgres

import (
	"context"
	"database/sql"
	"errors"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/spf13/cast"

	"app/genproto/kassa_service"
	"app/pkg/helper"
	"app/storage"
)

type SmenaRepo struct {
	db *pgxpool.Pool
}

func NewSmenaRepo(db *pgxpool.Pool) storage.SmenaRepoI {
	return &SmenaRepo{
		db: db,
	}
}

func (c *SmenaRepo) Create(ctx context.Context, req *kassa_service.CreateSmena) (resp *kassa_service.SmenaPrimaryKey, err error) {

	// TASK 8

	var new_smena_count int

	c.db.QueryRow(ctx, `SELECT COALESCE(COUNT(*), 0) FROM smena WHERE status = 'new' AND filial_id = $1`, req.FilialId).Scan(&new_smena_count)

	if new_smena_count > 0 {
		return nil, errors.New("У этого филиала уже есть открытая смена. Пожалуйста, сначала закройте его")
	}

	// #################################################################################################################################

	var id = uuid.New()

	var smena_code = "S-00000"
	var temp int

	err = c.db.QueryRow(ctx, `SELECT COALESCE(COUNT(*), 0) + 1 FROM smena`).Scan(&temp)
	if err != nil {
		return nil, err
	}

	smena_code += cast.ToString(temp)

	query := `INSERT INTO "smena" (
				id,
				smena_code,
				filial_id,
				employee_id,
				sale_point_id,
				updated_at
			) VALUES ($1, $2, $3, $4, $5, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		smena_code,
		req.GetFilialId(),
		req.GetEmployeeId(),
		req.GetSalePointId(),
	)

	if err != nil {
		return nil, err
	}

	return &kassa_service.SmenaPrimaryKey{Id: id.String()}, nil
}

func (c *SmenaRepo) GetByPKey(ctx context.Context, req *kassa_service.SmenaPrimaryKey) (resp *kassa_service.Smena, err error) {

	query := `
		SELECT
			id,
			smena_code,
			filial_id,
			employee_id,
			sale_point_id,
			status,
			created_at,
			updated_at,
			deleted_at
		FROM "smena"
		WHERE id = $1 and deleted_at is null
	`

	var (
		id            sql.NullString
		smena_code    sql.NullString
		filial_id     sql.NullString
		employee_id   sql.NullString
		sale_point_id sql.NullString
		status        sql.NullString
		createdAt     sql.NullString
		updatedAt     sql.NullString
		deleteddAt    sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.GetId()).Scan(
		&id,
		&smena_code,
		&filial_id,
		&employee_id,
		&sale_point_id,
		&status,
		&createdAt,
		&updatedAt,
		&deleteddAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &kassa_service.Smena{
		Id:          id.String,
		SmenaCode:   smena_code.String,
		FilialId:    filial_id.String,
		EmployeeId:  employee_id.String,
		SalePointId: sale_point_id.String,
		Status:      status.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
		DeletedAt:   deleteddAt.String,
	}

	return
}

func (c *SmenaRepo) GetAll(ctx context.Context, req *kassa_service.GetListSmenaRequest) (resp *kassa_service.GetListSmenaResponse, err error) {

	resp = &kassa_service.GetListSmenaResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " AND TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			smena_code,
			filial_id,
			employee_id,
			sale_point_id,
			status,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
			COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
		FROM "smena"
		WHERE COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')=''
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var smena kassa_service.Smena

		err := rows.Scan(
			&resp.Count,
			&smena.Id,
			&smena.SmenaCode,
			&smena.FilialId,
			&smena.EmployeeId,
			&smena.SalePointId,
			&smena.Status,
			&smena.CreatedAt,
			&smena.UpdatedAt,
			&smena.DeletedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Smenas = append(resp.Smenas, &smena)
	}

	return
}

func (c *SmenaRepo) Update(ctx context.Context, req *kassa_service.UpdateSmena) (rowsAffected int64, err error) {

	query := `
			UPDATE
			    "smena"
			SET
				filial_id     = $1,
				employee_id   = $2,
				sale_point_id = $3,
				smena_code 	  = $4,
				updated_at    = now()
			WHERE
				id = $6`

	result, err := c.db.Exec(ctx, query,
		req.GetFilialId(),
		req.GetEmployeeId(),
		req.GetSalePointId(),
		req.SmenaCode,
		req.GetId(),
	)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *SmenaRepo) Delete(ctx context.Context, req *kassa_service.SmenaPrimaryKey) error {

	query := `DELETE FROM "smena" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}

func (c *SmenaRepo) CloseSmena(ctx context.Context, req *kassa_service.SmenaPrimaryKey) (rowsAffected int64, err error) {

	res, err := c.db.Exec(ctx, `UPDATE smena SET status = 'closed' WHERE id = $1`, req.Id)
	if err != nil {
		return 0, errors.New("error in closing smena")
	}

	return res.RowsAffected(), nil
}
