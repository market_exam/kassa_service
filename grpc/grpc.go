package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"app/config"
	"app/genproto/kassa_service"
	"app/grpc/client"
	"app/grpc/service"
	"app/pkg/logger"
	"app/storage"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	kassa_service.RegisterSmenaServiceServer(grpcServer, service.NewSmenaService(cfg, log, strg, srvc))
	kassa_service.RegisterTransactionServiceServer(grpcServer, service.NewTransactionService(cfg, log, strg, srvc))
	kassa_service.RegisterSaleServiceServer(grpcServer, service.NewSaleService(cfg, log, strg, srvc))
	kassa_service.RegisterSaleProductServiceServer(grpcServer, service.NewSaleProductService(cfg, log, strg, srvc))
	kassa_service.RegisterSalePaymentServiceServer(grpcServer, service.NewSalePaymentService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
