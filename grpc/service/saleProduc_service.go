package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"app/config"
	"app/genproto/kassa_service"

	"app/grpc/client"
	"app/pkg/logger"
	"app/storage"
)

type SaleProductService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*kassa_service.UnimplementedSaleProductServiceServer
}

func NewSaleProductService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *SaleProductService {
	return &SaleProductService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *SaleProductService) Create(ctx context.Context, req *kassa_service.CreateSaleProduct) (resp *kassa_service.SaleProduct, err error) {

	i.log.Info("---CreateSaleProduct------>", logger.Any("req", req))

	pKey, err := i.strg.SaleProduct().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateSaleProduct->SaleProduct->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.SaleProduct().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeySaleProduct->saleProduct->GetByID--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (c *SaleProductService) GetById(ctx context.Context, req *kassa_service.SaleProductPrimaryKey) (resp *kassa_service.SaleProduct, err error) {

	c.log.Info("---GetSaleProductByID------>", logger.Any("req", req))

	resp, err = c.strg.SaleProduct().GetByPKey(ctx, req)
	if err != nil {
		c.log.Error("!!!GetSaleProductByID->SaleProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SaleProductService) GetList(ctx context.Context, req *kassa_service.GetListSaleProductRequest) (resp *kassa_service.GetListSaleProductResponse, err error) {

	i.log.Info("---GetSaleProduct------>", logger.Any("req", req))

	resp, err = i.strg.SaleProduct().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetSaleProduct->SaleProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SaleProductService) Update(ctx context.Context, req *kassa_service.UpdateSaleProduct) (resp *kassa_service.SaleProduct, err error) {

	i.log.Info("---UpdateSaleProductProduct------>", logger.Any("req", req))

	rowsAffected, err := i.strg.SaleProduct().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateSaleProduct--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.SaleProduct().GetByPKey(ctx, &kassa_service.SaleProductPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetSaleProduct->SaleProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *SaleProductService) Delete(ctx context.Context, req *kassa_service.SaleProductPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteSaleProduct------>", logger.Any("req", req))

	err = i.strg.SaleProduct().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteSaleProduct->SaleProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
