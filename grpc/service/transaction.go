package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"app/config"
	"app/genproto/kassa_service"

	"app/grpc/client"
	"app/pkg/logger"
	"app/storage"
)

type TransactionService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*kassa_service.UnimplementedTransactionServiceServer
}

func NewTransactionService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *TransactionService {
	return &TransactionService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *TransactionService) Create(ctx context.Context, req *kassa_service.CreateTransaction) (resp *kassa_service.Transaction, err error) {

	i.log.Info("---CreateTransaction------>", logger.Any("req", req))

	pKey, err := i.strg.Transaction().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateTransaction->Transaction->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Transaction().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyTransaction->Transaction->GetByID--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (c *TransactionService) GetById(ctx context.Context, req *kassa_service.TransactionPrimaryKey) (resp *kassa_service.Transaction, err error) {

	c.log.Info("---GetTransactionByID------>", logger.Any("req", req))

	resp, err = c.strg.Transaction().GetByPKey(ctx, req)
	if err != nil {
		c.log.Error("!!!GetTransactionByID->Transaction->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *TransactionService) GetList(ctx context.Context, req *kassa_service.GetListTransactionRequest) (resp *kassa_service.GetListTransactionResponse, err error) {

	i.log.Info("---GetTransaction------>", logger.Any("req", req))

	resp, err = i.strg.Transaction().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetTransaction->Transaction->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *TransactionService) Delete(ctx context.Context, req *kassa_service.TransactionPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteTransaction------>", logger.Any("req", req))

	err = i.strg.Transaction().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteTransaction->Transaction->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}

// func (i *SmenaService) Update(ctx context.Context, req *kassa_service.UpdateSmena) (resp *kassa_service.Smena, err error) {

// 	i.log.Info("---UpdateSmena------>", logger.Any("req", req))

// 	rowsAffected, err := i.strg.Smena().Update(ctx, req)

// 	if err != nil {
// 		i.log.Error("!!!UpdateSmena--->", logger.Error(err))
// 		return nil, status.Error(codes.InvalidArgument, err.Error())
// 	}

// 	if rowsAffected <= 0 {
// 		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
// 	}

// 	resp, err = i.strg.Smena().GetByPKey(ctx, &kassa_service.SmenaPrimaryKey{SmenaId: req.SmenaId})
// 	if err != nil {
// 		i.log.Error("!!!GetSmena->Smena->Get--->", logger.Error(err))
// 		return nil, status.Error(codes.NotFound, err.Error())
// 	}

// 	return resp, err
// }
